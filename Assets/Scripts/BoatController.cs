﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoatController : MonoBehaviour {

    enum MovementType
    {
        Standard,
        TankControls
    }
    [SerializeField] MovementType m_MovementType;

    // p1, p2, p3, p4 etc
    [HideInInspector]
    public int m_playerNum;

    [SerializeField] float m_MovementSpeed = 2f;

    [SerializeField]
    float m_DashDistance = 25f;
    [SerializeField]
    float m_DashCoolDownTime = 2f;

    [Header("Tank Controls")]
    // Applies only to Tank Controls
    [SerializeField]
    float m_BackwardsSpeed = 1f;
    [SerializeField]
    float m_TurnSpeed = 5f;

    [Header("Physics Based")]
    [SerializeField]
    bool m_EnablePhysicsLocomotion = false;
    [SerializeField]
    float m_PhysicsMovementScale = 20f;
    [SerializeField]
    float m_maxPhysicsVelocity = 10f;
    [SerializeField]
    float m_DashForce = 2f;

    [Header("Boat Sound Effects")]
    public AudioClip m_DashSound;
    public AudioClip m_HitSound;
    public AudioClip m_PickupSound;

    [Header("Pickup & Dropoff")]
    [SerializeField]
    float m_PickupRadius = 25f;

    private Rigidbody m_Rigidbody;
    private AudioSource m_AudioSource;
    private BoatInventory m_BoatInventory;

    float m_TurnAmount;
    float m_ForwardAmount;

    [HideInInspector]
    public bool m_isDashing = false;
    [SerializeField]
    float m_DashDuration = 1f;
    [SerializeField]
    float m_DashWaitTime = 1f;
    float m_curDashTimer = 0f;

    // Use this for initialization
    void Start()
    {
        m_Rigidbody = GetComponent<Rigidbody>();
        m_AudioSource = GetComponent<AudioSource>();
        m_BoatInventory = GetComponent<BoatInventory>();
    }

    private void Update()
    {
        if (m_curDashTimer > 0f)
        {
            m_curDashTimer -= Time.deltaTime;

            if (m_curDashTimer <= m_DashWaitTime - m_DashDuration)
            {
                m_isDashing = false;
            }
        }
    }

    // Moves the boat based on input sent from the InputController
    public void Move(Vector3 movementVector)
    {
        //if (movementVector.magnitude > 1f) movementVector.Normalize();
        //movementVector = transform.InverseTransformDirection(movementVector);
        //movementVector = Vector3.ProjectOnPlane(movementVector, Vector3.up);

        // Prevents boat from snapping back to its original rotation
        if (m_MovementType == MovementType.Standard && movementVector != Vector3.zero)
        {
            if (m_EnablePhysicsLocomotion)
            {
                m_Rigidbody.AddForce(new Vector3(movementVector.x, 0, movementVector.z) * m_PhysicsMovementScale, ForceMode.Force);
                m_Rigidbody.velocity = Vector3.ClampMagnitude(m_Rigidbody.velocity, m_maxPhysicsVelocity);

                //Debug.Log("Current Velocity: " + m_Rigidbody.velocity.magnitude);

                //transform.rotation = Quaternion.LookRotation(movementVector);
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(movementVector), 0.2f);
            }
            else
            {
                transform.position += new Vector3(movementVector.x, 0, movementVector.z) * m_MovementSpeed;

                //transform.rotation = Quaternion.LookRotation(movementVector);
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(movementVector), 0.2f);
            }
        }

        if (m_MovementType == MovementType.TankControls)
        {
            float currentSpeed = 0f;

            if (movementVector.z > 0)
            {
                currentSpeed = m_MovementSpeed;
            }
            else
            {
                currentSpeed = 0; //m_BackwardsSpeed;
            }

            if (m_EnablePhysicsLocomotion)
            {
                m_Rigidbody.AddForce(transform.forward * movementVector.z * m_PhysicsMovementScale, ForceMode.Force);

                transform.Rotate(0, movementVector.x * m_TurnSpeed, 0);
            }
            else
            {
                transform.position += (transform.forward * movementVector.z) * currentSpeed;
                transform.Rotate(0, movementVector.x * m_TurnSpeed, 0);
            }
        }
    }

    // Pick up the peons
    public void PickUp()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, m_PickupRadius);

        Collider nearestCollider = null;
        float minSqrDistance = Mathf.Infinity;

        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].GetComponent<PickupNPCs>() != null)
            {
                // Don't allow it to try to pick up and NPC it already has in its inventory
                if (!m_BoatInventory.m_Occupants.Contains(colliders[i].GetComponent<PickupNPCs>()))
                {
                    float sqrDistanceToCenter = (transform.position - colliders[i].transform.position).sqrMagnitude;

                    if (sqrDistanceToCenter < minSqrDistance)
                    {
                        minSqrDistance = sqrDistanceToCenter;
                        nearestCollider = colliders[i];
                    }
                }
            }
        }

        // The boat will pick up the nearest pickup npc
        if (nearestCollider != null)
        {
            PickupNPCs npc = nearestCollider.GetComponent<PickupNPCs>();

            m_BoatInventory.AddOccupantToInvent(npc);
            npc.PickedUp(this);

            m_AudioSource.PlayOneShot(m_PickupSound);
        }
    }

    // Drop off the peons
    public void DropOff()
    {
        if (m_BoatInventory.m_Occupants.Count > 0)
        {
            PickupNPCs npc = m_BoatInventory.RemoveOccupantFromInvent();

            // Enable physics on the npcs again
            npc.Dropped();

            npc.transform.position = transform.position + transform.forward * 10f + new Vector3(0, 10, 0);
            npc.GetComponent<Rigidbody>().AddForce(60f * transform.forward, ForceMode.Impulse);
            npc.GetComponent<Rigidbody>().AddForce(60f * Vector3.up, ForceMode.Impulse);
        }
    }

    // If another players boosts into this boat or they both boost into each other, they lose all of their passengers
    public void DropAll()
    {
        foreach(PickupNPCs occupant in m_BoatInventory.m_Occupants)
        {
            PickupNPCs npc = m_BoatInventory.RemoveOccupantFromInvent();
            // Enable physics on the npcs again
            npc.Dropped();
        }
    }

    // Boost
    public void Dash()
    {
        //Debug.Log("Dashing");
        if (!m_isDashing && m_curDashTimer <= 0)
        {
            if (m_EnablePhysicsLocomotion)
            {
                m_Rigidbody.AddForce(transform.forward * m_DashForce, ForceMode.Impulse);
            }
            else
            {
                transform.position += transform.forward * 25f;
            }

            m_isDashing = true;
            m_curDashTimer = m_DashWaitTime;

            // Play dash sound effect
            m_AudioSource.PlayOneShot(m_DashSound);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<BoatController>() != null)
        {
            BoatController otherBoat = collision.gameObject.GetComponent<BoatController>();

            if (m_isDashing)
            {
                otherBoat.DropAll();
                otherBoat.GetComponent<Rigidbody>().AddForce(transform.forward * 150f, ForceMode.Impulse);
                m_AudioSource.PlayOneShot(m_HitSound);
            }

            //if (otherBoat.m_isDashing)
            //{
            //    DropAll();
            //}
        }
    }
}
