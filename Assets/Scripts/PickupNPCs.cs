﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupNPCs : MonoBehaviour {

    [SerializeField] float m_DrownTimer = 3f;
    private float m_curDrownTimer = 0f;

    // NPCs position when the game begins
    private Vector3 m_InitPos;
    private BoatController m_ParentBoat;

    private Collider m_Collider;
    private Rigidbody m_Rigidbody;

    // On Pickup changed status to picked up and holds a reference to the player holding it
    public enum NPCType
    {
        Green,
        Brown,
        Pink
    }
    public NPCType m_NPCType;

    public enum LocationStatus
    {
        Land,
        PlayerInventory,
        Water
    }
    public LocationStatus m_LocationStatus;

	// Use this for initialization
	void Start ()
    {
        m_InitPos = transform.position;

        m_Collider = GetComponent<Collider>();
        m_Rigidbody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        /*
        if (m_LocationStatus != LocationStatus.PlayerInventory && m_LocationStatus != LocationStatus.Water)
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, Vector3.down, out hit))
            {
                if (Vector3.Distance(transform.position, hit.point) < 1f)
                {
                    if (hit.transform.gameObject.name == "Water Plane")
                    {
                        Debug.Log("Hit water");
                        m_LocationStatus = LocationStatus.Water;
                        m_curDrownTimer = m_DrownTimer;
                    }
                }
            }
        }
        */
        
        if (m_LocationStatus == LocationStatus.Water)
        {
            m_curDrownTimer -= Time.deltaTime;

            if (m_curDrownTimer <= 0f)
            {
               Respawn();
            }
        }
        
	}

    // Called by the BoatController when it picks the PickupNPC up
    // Registers a reference to the BoatController and hides the PickupNPC until thrown or they fall out
    public void PickedUp(BoatController newBoatParent)
    {
        m_Collider.isTrigger = true;
        m_Rigidbody.isKinematic = true;

        m_ParentBoat = newBoatParent;
        m_LocationStatus = LocationStatus.PlayerInventory;
    }

    public void Dropped()
    {
        m_Collider.isTrigger = false;
        m_Rigidbody.isKinematic = false;

        m_LocationStatus = LocationStatus.Land;
        //m_LocationStatus = LocationStatus.Released;
    }

    private void OnTriggerEnter(Collider collider)
    {
		//Debug.Log("npc has COLLIDED WITH A THING!" + collider.gameObject.ToString());
        if (collider.gameObject.GetComponent<DeliveryLocation>())
        {
        //Debug.Log("npc has entered a collision bubble!");
			if(collider.gameObject.GetComponent<DeliveryLocation>() != null
                && collider.gameObject.GetComponent<DeliveryLocation>().DeliveryLocationNumber == GameController.Instance.getCurrentTaskLocation())
            {
//				Debug.Log("npc has entered the winning collision bubble!! it is a " + m_NPCType + " and the winning one is a " + GameController.Instance.getCurrentNPCType());
				if(m_NPCType == GameController.Instance.getCurrentNPCType() && m_LocationStatus == LocationStatus.Land)
                {
					GameController.Instance.UpdatePlayerScore(m_ParentBoat.m_playerNum, 10);

				}
	        }
	    }
    }

    
    private void OnCollisionEnter(Collision collision)
    {
        if (m_LocationStatus != LocationStatus.PlayerInventory && m_LocationStatus != LocationStatus.Water)
        {
            if (collision.collider.gameObject.name == "Water Plane")
            {
                Debug.Log("Hit water");
                m_LocationStatus = LocationStatus.Water;
                m_curDrownTimer = m_DrownTimer;

                m_Rigidbody.isKinematic = true;
                m_Collider.isTrigger = true;
            }
        }

    }
    

    void Respawn()
    {
        transform.position = m_InitPos;
        m_LocationStatus = LocationStatus.Land;
    }

}
