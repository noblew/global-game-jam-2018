﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameController : Singleton<GameController> {

    protected GameController() { }

    public Text[] scoresTextList;
    public Text turnTimer;
    public Text tasksLeftCounter;
    public Text divineTaskText;

    public DeliveryLocation[] deliveryLocationList;

    private int[] scoresList;

    public int numberOfTasks;
    public int timePerTask;

    private float taskTimer;
    private float tasksLeft;

    private bool gameRunning;

//    public int[] player_scores;

	private DivineTask currentTask;

    void Awake ()
    {
    	gameRunning = true;
    	taskTimer = timePerTask;
		tasksLeft = numberOfTasks;
        scoresList = new int[4];
		PickupNPCs.NPCType nextNPCType = (PickupNPCs.NPCType)Random.Range(0, System.Enum.GetNames(typeof(PickupNPCs.NPCType)).Length);
        int nextLocation = Random.Range(0, deliveryLocationList.Length);
        while (nextLocation == (int)nextNPCType)
        {
            nextLocation = Random.Range(0, deliveryLocationList.Length);
        }
		currentTask = new DivineTask(nextNPCType, deliveryLocationList[nextLocation]);
        
		Debug.Log( currentTask.getCurrentCreature() + " = current creature, " + currentTask.getCurrentLocation().DeliveryLocationNumber + " = current location");
    }

    void FixedUpdate () {
    	

		if (Input.GetKeyDown("1"))
			UpdatePlayerScore(0, 10);
		if (Input.GetKeyDown("2"))
			UpdatePlayerScore(1, 10);
		if (Input.GetKeyDown("3"))
			UpdatePlayerScore(2, 10);
		if (Input.GetKeyDown("4"))
			UpdatePlayerScore(3, 10);

		if (CheckGameOver()) {
			GameOver();
		}
		SyncScoresToUI();
		UpdateTimer();
    }

    void UpdateTimer() {
		taskTimer -= Time.deltaTime;
		if (taskTimer <= 0.0f)
		{
			timerEnded();
		}
    }

    void timerEnded() {
    	if (gameRunning) {
	    	tasksLeft--;
			taskTimer = timePerTask;
			PickupNPCs.NPCType nextNPCType = (PickupNPCs.NPCType)Random.Range(0, System.Enum.GetNames(typeof(PickupNPCs.NPCType)).Length);

			currentTask = new DivineTask(nextNPCType, deliveryLocationList[Random.Range(0, deliveryLocationList.Length)]);
		}
    }

    public void UpdatePlayerScore(int playerNumber, int score) {
    	Debug.Log("playerNumber = " + playerNumber);
    	scoresList[playerNumber] += score;
    }

	void SyncScoresToUI() {
		if (gameRunning) {
	    	for(int i = 0; i < 4; i++) {
				scoresTextList[i].text = scoresList[i].ToString();
	    	}
			turnTimer.text = Mathf.RoundToInt(taskTimer).ToString();
			tasksLeftCounter.text = tasksLeft.ToString();
			divineTaskText.text = currentTask.getCurrentTaskString();
		}
    }

    public int getCurrentTaskLocation() {
    	return currentTask.getCurrentLocation().DeliveryLocationNumber;
    }

	public PickupNPCs.NPCType getCurrentNPCType() {
		return currentTask.getCurrentCreature();
    }

    public bool CheckGameOver() {
    	return (tasksLeft < 0);
    }

    public int currentWinner() {
		return System.Array.IndexOf(scoresList, Mathf.Max(scoresList));
    }

    void GameOver ()
    {
		divineTaskText.text = "THE GAME IS OVER!";
//		Debug.Log("Game has ended");
		gameRunning = false;
		GameStateManager.Instance.EndGame();

    }
}