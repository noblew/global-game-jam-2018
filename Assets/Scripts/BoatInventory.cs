﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoatInventory : MonoBehaviour
{
    [SerializeField]
    Transform[] m_OccupantBoatPositions = new Transform[5];


	private int m_BoatCapacity = 5;
	private int m_OccupantNumber = 0;
	private PickupNPCs.NPCType m_OccupantType;
    public List<PickupNPCs> m_Occupants;

    // Adds the occupant to our list
    // If you already have an occupant, it only captures occupants of the same type
    public void AddOccupantToInvent(PickupNPCs npc)
    {
        if (m_Occupants.Count <= 0)
        {
            m_OccupantType = npc.m_NPCType;
            m_Occupants.Add(npc);
            InviteOccupant(npc);
        }
        else if (m_Occupants.Count < m_BoatCapacity && npc.m_NPCType == m_OccupantType)
        {
            m_Occupants.Add(npc);
            InviteOccupant(npc);
        }
    }

    // Returns an occupan to be dealt with accordingly
    public PickupNPCs RemoveOccupantFromInvent()
    {
        if (m_Occupants.Count <= 0)
        {
            return null;
        }
        else
        {
            KickOutOccupant(m_Occupants[m_Occupants.Count - 1]);
            PickupNPCs outcast = m_Occupants[m_Occupants.Count - 1];
            m_Occupants.Remove(outcast);

            return outcast;
        }
    }

    // Parent and show the occupant
    void InviteOccupant(PickupNPCs npc)
    {
        npc.transform.parent = this.transform;

        // If we aleady added an occupant we should be safe to move it
        npc.transform.position = m_OccupantBoatPositions[m_Occupants.Count - 1].position;
    }

    // Unparent the occupant so it can get ready to be thrown
    void KickOutOccupant(PickupNPCs npc)
    {
        // GET OFF MY LAWN!!!
        npc.transform.parent = null;
    }

	// Use this for initialization
	void Start ()
    {

	}

	public int BoatCapacity
	{
		get
		{
			return m_BoatCapacity;
		}

	}

	public int OccupantNumber
	{
		get
		{
			return m_OccupantNumber;
		}

	}
	public PickupNPCs.NPCType OccupantType
	{
		get
		{
			return m_OccupantType;
		}

		set
		{
			m_OccupantType = value;
		}

	}
}
