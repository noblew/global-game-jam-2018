﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class DivineTask {

	public string taskText;

	private PickupNPCs.NPCType currentCreature;
	private DeliveryLocation currentLocation;

	public DivineTask(PickupNPCs.NPCType creature, DeliveryLocation location) {
		currentCreature = creature;
		currentLocation = location;
	}

	public PickupNPCs.NPCType getCurrentCreature() {
		return currentCreature;
	}

	public DeliveryLocation getCurrentLocation() {
		return currentLocation;
	}

	public string getCurrentTaskString() {
		string currentTask = "BRING THE ";
		currentTask += currentCreature.ToString().ToUpper();
		currentTask += " ONES TO THE ";

		currentTask += currentLocation.DeliveryLocationName.ToUpper() + "! APPEASE ME!";
		return currentTask;
	}
}
