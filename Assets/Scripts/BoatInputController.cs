﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using InControl;

[RequireComponent(typeof(BoatController))]
public class BoatInputController : MonoBehaviour
{
    [SerializeField] int m_PlayerNumber = 0;

    [HideInInspector]
    public InputDevice m_GamePad;

    // With InControl, the keyboard input is noticably slower than the joystick input, this is as temp fix
    [SerializeField] float m_KeyboardInputScale = 2f;

    private BoatController m_BoatController;
    private Transform m_Cam;
    private Vector3 m_CamForward;

    private Vector3 m_Move;
    //private bool m_Boost;
    //private bool m_Pickup;
    //private bool m_DropOff;
    

	// Use this for initialization
	void Start ()
    {
        //if (InputManager.Devices.Count < m_PlayerNumber + 1)
        {
            //foreach (InputDevice device in InputManager.Devices)
            //{
            //    //if (device.)
            //}

            //m_GamePad = InputManager.Devices[m_PlayerNumber];
            //m_GamePad = InputManager.ActiveDevice;
        }

        // get the transform of the main camera
        if (Camera.main != null)
        {
            m_Cam = Camera.main.transform;
        }

        m_BoatController = GetComponent<BoatController>();
    }

    // Update is called once per frame
    void FixedUpdate ()
    {
        //m_GamePad = GameStateManager.Instance.RemapInput(m_GamePadIndex);

        //if (InputManager.Devices.Count < m_PlayerNumber + 1)
        {
           // m_GamePad = InputManager.ActiveDevice; //InputManager.Devices[m_PlayerNumber];
        }

        if (m_GamePad != null && m_GamePad.IsAttached)
        {
            CheckJoystickInput();
            CheckButtonInput();
        }

    }

    // Checks the input on the left-joystick and WASD
    void CheckJoystickInput()
    {
        float horiz = 0f;
        float vert = 0f;

        if (m_GamePad != null)
        {
            // read inputs
            horiz = m_GamePad.LeftStickX;
            vert = m_GamePad.LeftStick.Y;
        }

        // The first player should be able to receive keyboard input
        if (m_PlayerNumber == 0 && GameStateManager.Instance.m_InputDevices.Count <= 1)
        {
           // horiz += CrossPlatformInputManager.GetAxis("Horizontal") * m_KeyboardInputScale;
           // vert += CrossPlatformInputManager.GetAxis("Vertical") * m_KeyboardInputScale;
        }

        // calculate move direction to pass to character
        if (m_Cam != null)
        {
            // calculate camera relative direction to move:
            m_CamForward = Vector3.Scale(m_Cam.forward, new Vector3(1, 0, 1)).normalized;
            m_Move = vert * m_CamForward + horiz * m_Cam.right;
        }
        else
        {
            // we use world-relative directions in the case of no main camera
            m_Move = vert * Vector3.forward + horiz * Vector3.right;
        }

        if (m_BoatController != null)
        {
            m_BoatController.Move(m_Move);
        }
    }

    void CheckButtonInput()
    {
        // PickUp
        if (Input.GetKeyDown(KeyCode.E) || (m_GamePad != null && m_GamePad.RightTrigger.WasPressed))
        {
            if (m_BoatController != null)
            {
                m_BoatController.PickUp();
            }
        }

        // Dropoff
        if (Input.GetKeyDown(KeyCode.Q) || (m_GamePad != null && m_GamePad.LeftTrigger.WasPressed))
        {

            if (m_BoatController != null)
            {
                m_BoatController.DropOff();
            }
        }

        //Dash
        if (Input.GetKeyDown(KeyCode.Space) || (m_GamePad != null && m_GamePad.Action1.WasPressed))
        {
            if (m_BoatController != null)
            {
                m_BoatController.Dash();
            }
        }
    }
}
