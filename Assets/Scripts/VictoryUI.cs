﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;


public class VictoryUI : MonoBehaviour
{
    public AudioClip m_VictorySound;

    void Awake()
    {
        GetComponent<AudioSource>().PlayOneShot(m_VictorySound);
    }

    public void RematchButtonClicked()
    {
        Initiate.Fade("Main", Color.black, 2.0f);
    }

    public void ExitButtonClicked()
    {
        Initiate.Fade("StartMenu", Color.black, 2.0f);
    }
}