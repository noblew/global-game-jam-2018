﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InControl;
using UnityEngine.SceneManagement;

public class GameStateManager : Singleton<GameStateManager>
{
    protected GameStateManager() { }

    //float m_currentGameTime;

    public GameObject m_BoatPrefab;
    [HideInInspector]
    public List<InputDevice> m_InputDevices = new List<InputDevice>();
    [HideInInspector]
    public BoatController[] m_Players = new BoatController[4];
    // Manually set spawn points in the world for each player
    public Transform[] m_PlayerSpawnPoints = new Transform[4];

    // Set to true when end condition is reached
    // Dont Destroy this object on scene load because it is needed to set the text on the winning UI for the winning player
    public int m_winnningPlayerNum;
    int m_VictoryMenuScene = 2;

    // Use this for initialization
    void Start ()
    {
        InputManager.OnActiveDeviceChanged += inputDevice => Debug.Log("Active Device Changed: " + inputDevice.Name);
        InputManager.OnActiveDeviceChanged += OnInputDeviceChanged;
    }

    private void OnDestroy()
    {
        InputManager.OnActiveDeviceChanged -= OnInputDeviceChanged;
    }

    // Update is called once per frame
    void Update ()
    {
        //m_currentGameTime = Time.timeSinceLevelLoad;

        if (Input.GetKeyDown(KeyCode.V))
        {
            EndGame();
        }
	}

    void OnInputDeviceChanged(InputDevice device)
    {
        if (m_InputDevices.Count == 0)
        {
            SpawnPlayer(device);
            Debug.Log("We've added first player");
        }
        else
        {
            foreach (InputDevice player in m_InputDevices)
            {
                if (player == device)
                {
                    break;
                }
                else if (m_InputDevices.IndexOf(player) == m_InputDevices.Count - 1)
                {
                    SpawnPlayer(device);
                    // Spawn Player
                    Debug.Log("We've added a player");
                }
                Debug.Log("Total Players: " + m_InputDevices.Count);
            }
        }
    }

    void SpawnPlayer(InputDevice device)
    {
        if (m_InputDevices.Count <= 4)
        {
            m_InputDevices.Add(device);

            // Spawn a new player boat and set its position
            GameObject newBoat = GameObject.Instantiate(m_BoatPrefab);
            newBoat.transform.position = m_PlayerSpawnPoints[m_InputDevices.Count - 1].position;

            BoatInputController newBoatInputController = newBoat.GetComponent<BoatInputController>();

            if (newBoatInputController != null && newBoatInputController.m_GamePad == null)
            {
                newBoatInputController.m_GamePad = device;
            }

            BoatController playerRef = newBoat.GetComponent<BoatController>();

            if (playerRef != null)
            {
                m_Players[m_InputDevices.Count - 1] = playerRef;
                playerRef.m_playerNum = m_InputDevices.Count - 1;
            }
        }
    }

    public void EndGame()
    {
        m_winnningPlayerNum = GameController.Instance.currentWinner();

		if (m_winnningPlayerNum == 0) {
			Initiate.Fade("VictoryMenu_p1", Color.black, 2.0f);
		} else if (m_winnningPlayerNum == 1){
			Initiate.Fade("VictoryMenu_p2", Color.black, 2.0f);
		} else if (m_winnningPlayerNum == 2){
			Initiate.Fade("VictoryMenu_p3", Color.black, 2.0f);
		} else {
			Initiate.Fade("VictoryMenu_p4", Color.black, 2.0f);
		}
    }
}
