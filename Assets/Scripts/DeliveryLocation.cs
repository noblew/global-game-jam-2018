﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeliveryLocation : MonoBehaviour {

	public int deliveryLocationNumber;
	public string deliveryLocationName;

	void OnTriggerEnter (Collider other) {
		Debug.Log("Object entered the trigger! It is a: " + other.ToString());
	}

	public int DeliveryLocationNumber
	{
		get
		{
			return deliveryLocationNumber;
		}

	}

	public string DeliveryLocationName
	{
		get
		{
			return deliveryLocationName;
		}

	}
}
