﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
Ideal behavior:
    * Movement due to player input is restricted to forward in the Z
    * Turning is done at a certain rate, toward a target angle
    * Turning rate is inversely proportional to speed
    * Turning should have some acceleration
    * Constant drag killing forward momentum
    * Bank the ship when turning at fast speeds
    * 
*/


public class JT_BoatController : MonoBehaviour
{
    public float thrust;
    public float max_speed;
    public float max_rotation;
    public float at_speed_rotation;
    public float torque;
    private Rigidbody rb;
    public float default_drag;
    public float moving_drag;


    // 


    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>();
        rb.drag = default_drag;
    }
	
	// Update is called once per frame
	void Update () {
        if (rb.velocity.z < max_speed && Input.GetAxis("Vertical") > 0)
        {
            rb.AddForce(transform.forward * thrust * Input.GetAxis("Vertical") * 2, ForceMode.Force);
            rb.drag = moving_drag;
        }
        else if (Input.GetAxis("Vertical") <= 0)
            rb.drag = default_drag;

        // Torque to be added should be inversely proportional to the current velocity magnitude
        // plus a certain amount so that you can still turn a little bit when you're going at top speed
        rb.maxAngularVelocity = max_rotation;
        Vector3 t_vector = (transform.up * (torque * (1 - (rb.velocity.z / (max_speed + at_speed_rotation)))) * Input.GetAxis("Horizontal"));
        rb.AddTorque(t_vector, ForceMode.Force); // Rotate the ship
    }
}
