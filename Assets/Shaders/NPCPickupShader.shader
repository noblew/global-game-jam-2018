// Shader created with Shader Forge v1.37 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.37;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:33310,y:32720,varname:node_3138,prsc:2|emission-2121-OUT,olwid-5930-OUT,olcol-4364-RGB;n:type:ShaderForge.SFN_Color,id:7241,x:32503,y:32224,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_7241,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.07843138,c2:0.3921569,c3:0.7843137,c4:1;n:type:ShaderForge.SFN_ValueProperty,id:2642,x:32273,y:32960,ptovrint:False,ptlb:Outline Width,ptin:_OutlineWidth,varname:node_2642,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.05;n:type:ShaderForge.SFN_Color,id:4364,x:32382,y:33206,ptovrint:False,ptlb:Outline Color,ptin:_OutlineColor,varname:node_4364,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.1172414,c2:1,c3:0,c4:1;n:type:ShaderForge.SFN_Time,id:6391,x:31828,y:32988,varname:node_6391,prsc:2;n:type:ShaderForge.SFN_Sin,id:2316,x:32031,y:33012,varname:node_2316,prsc:2|IN-6391-TTR;n:type:ShaderForge.SFN_Add,id:5930,x:32421,y:33048,varname:node_5930,prsc:2|A-2642-OUT,B-5687-OUT;n:type:ShaderForge.SFN_Multiply,id:5687,x:32247,y:33048,varname:node_5687,prsc:2|A-2316-OUT,B-7805-OUT;n:type:ShaderForge.SFN_Fresnel,id:9738,x:32531,y:32619,varname:node_9738,prsc:2|NRM-2390-OUT,EXP-8600-OUT;n:type:ShaderForge.SFN_Lerp,id:2121,x:32889,y:32504,varname:node_2121,prsc:2|A-517-OUT,B-7241-RGB,T-9738-OUT;n:type:ShaderForge.SFN_ValueProperty,id:8600,x:32305,y:32693,ptovrint:False,ptlb:Fresnel Power,ptin:_FresnelPower,varname:node_8600,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.5;n:type:ShaderForge.SFN_NormalVector,id:2390,x:32305,y:32519,prsc:2,pt:False;n:type:ShaderForge.SFN_Color,id:8096,x:32503,y:32393,ptovrint:False,ptlb:Fresnel Color,ptin:_FresnelColor,varname:node_8096,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.4059778,c2:0.3622405,c3:0.7352941,c4:1;n:type:ShaderForge.SFN_Multiply,id:517,x:32770,y:32290,varname:node_517,prsc:2|A-7241-RGB,B-8096-RGB;n:type:ShaderForge.SFN_ValueProperty,id:7805,x:32031,y:33263,ptovrint:False,ptlb:Outline Pulse Amount,ptin:_OutlinePulseAmount,varname:node_7805,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.1;proporder:7241-2642-4364-8600-8096-7805;pass:END;sub:END;*/

Shader "Shader Forge/NPCPickupShader" {
    Properties {
        _Color ("Color", Color) = (0.07843138,0.3921569,0.7843137,1)
        _OutlineWidth ("Outline Width", Float ) = 0.05
        _OutlineColor ("Outline Color", Color) = (0.1172414,1,0,1)
        _FresnelPower ("Fresnel Power", Float ) = 0.5
        _FresnelColor ("Fresnel Color", Color) = (0.4059778,0.3622405,0.7352941,1)
        _OutlinePulseAmount ("Outline Pulse Amount", Float ) = 0.1
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "Outline"
            Tags {
            }
            Cull Front
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float _OutlineWidth;
            uniform float4 _OutlineColor;
            uniform float _OutlinePulseAmount;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                float4 node_6391 = _Time + _TimeEditor;
                o.pos = UnityObjectToClipPos( float4(v.vertex.xyz + v.normal*(_OutlineWidth+(sin(node_6391.a)*_OutlinePulseAmount)),1) );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                return fixed4(_OutlineColor.rgb,0);
            }
            ENDCG
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _Color;
            uniform float _FresnelPower;
            uniform float4 _FresnelColor;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos( v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float3 emissive = lerp((_Color.rgb*_FresnelColor.rgb),_Color.rgb,pow(1.0-max(0,dot(i.normalDir, viewDirection)),_FresnelPower));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
