// Shader created with Shader Forge v1.37 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.37;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:33203,y:32875,varname:node_3138,prsc:2|emission-9034-OUT,voffset-2730-OUT;n:type:ShaderForge.SFN_Color,id:7241,x:32094,y:32733,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_7241,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.07843138,c2:0.3921569,c3:0.7843137,c4:1;n:type:ShaderForge.SFN_Tex2d,id:6043,x:32083,y:32932,varname:node_6043,prsc:2,tex:9246e4104d9345945986a1d166ed83af,ntxv:0,isnm:False|UVIN-5965-OUT,TEX-417-TEX;n:type:ShaderForge.SFN_Tex2dAsset,id:417,x:31866,y:33020,ptovrint:False,ptlb:Color Ramp,ptin:_ColorRamp,varname:node_417,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:9246e4104d9345945986a1d166ed83af,ntxv:0,isnm:False;n:type:ShaderForge.SFN_AmbientLight,id:3748,x:32356,y:32736,varname:node_3748,prsc:2;n:type:ShaderForge.SFN_Dot,id:8544,x:31563,y:32669,varname:node_8544,prsc:2,dt:4|A-6011-OUT,B-1272-OUT;n:type:ShaderForge.SFN_LightVector,id:6011,x:31381,y:32669,varname:node_6011,prsc:2;n:type:ShaderForge.SFN_NormalVector,id:1272,x:31381,y:32796,prsc:2,pt:False;n:type:ShaderForge.SFN_Append,id:5965,x:31744,y:32841,varname:node_5965,prsc:2|A-8544-OUT,B-3880-OUT;n:type:ShaderForge.SFN_Vector1,id:3880,x:31563,y:32841,varname:node_3880,prsc:2,v1:0;n:type:ShaderForge.SFN_Multiply,id:9007,x:32327,y:32907,varname:node_9007,prsc:2|A-7241-RGB,B-6043-RGB;n:type:ShaderForge.SFN_Fresnel,id:2437,x:31555,y:33274,varname:node_2437,prsc:2|NRM-1272-OUT,EXP-8997-OUT;n:type:ShaderForge.SFN_Slider,id:8997,x:31139,y:33346,ptovrint:False,ptlb:Fresnel Exp,ptin:_FresnelExp,varname:node_8997,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1.572,max:15;n:type:ShaderForge.SFN_Add,id:5750,x:32277,y:33083,varname:node_5750,prsc:2|A-9007-OUT,B-147-OUT;n:type:ShaderForge.SFN_OneMinus,id:9597,x:31647,y:33051,varname:node_9597,prsc:2|IN-8544-OUT;n:type:ShaderForge.SFN_Lerp,id:147,x:32007,y:33245,varname:node_147,prsc:2|A-1179-OUT,B-6482-OUT,T-9597-OUT;n:type:ShaderForge.SFN_Vector1,id:1179,x:31668,y:33201,varname:node_1179,prsc:2,v1:0;n:type:ShaderForge.SFN_Lerp,id:6482,x:31870,y:33350,varname:node_6482,prsc:2|A-1179-OUT,B-2482-RGB,T-2437-OUT;n:type:ShaderForge.SFN_Color,id:2482,x:31528,y:33497,ptovrint:False,ptlb:Fresnel Color,ptin:_FresnelColor,varname:node_2482,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.3455882,c2:0.8646043,c3:1,c4:1;n:type:ShaderForge.SFN_Tex2d,id:3367,x:32588,y:33511,ptovrint:False,ptlb:Displacement Noise,ptin:_DisplacementNoise,varname:node_3367,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:28c7aad1372ff114b90d330f8a2dd938,ntxv:0,isnm:False|UVIN-9849-OUT;n:type:ShaderForge.SFN_Panner,id:7403,x:32198,y:33516,varname:node_7403,prsc:2,spu:1,spv:1|UVIN-6885-UVOUT,DIST-7613-OUT;n:type:ShaderForge.SFN_TexCoord,id:6885,x:31893,y:33509,varname:node_6885,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Time,id:9633,x:31868,y:33734,varname:node_9633,prsc:2;n:type:ShaderForge.SFN_Multiply,id:2730,x:32770,y:33509,varname:node_2730,prsc:2|A-3367-R,B-5520-OUT;n:type:ShaderForge.SFN_Slider,id:5520,x:32431,y:33705,ptovrint:False,ptlb:Offset Depth,ptin:_OffsetDepth,varname:node_5520,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:2;n:type:ShaderForge.SFN_Multiply,id:7613,x:32080,y:33798,varname:node_7613,prsc:2|A-9633-T,B-7316-OUT;n:type:ShaderForge.SFN_Slider,id:7316,x:31662,y:33918,ptovrint:False,ptlb:Wind speed,ptin:_Windspeed,varname:node_7316,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.4615385,max:5;n:type:ShaderForge.SFN_Multiply,id:9849,x:32400,y:33546,varname:node_9849,prsc:2|A-7403-UVOUT,B-469-OUT;n:type:ShaderForge.SFN_Slider,id:469,x:32221,y:33845,ptovrint:False,ptlb:Noise Size Mult,ptin:_NoiseSizeMult,varname:node_469,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.2,max:1;n:type:ShaderForge.SFN_LightAttenuation,id:4496,x:32386,y:33371,varname:node_4496,prsc:2;n:type:ShaderForge.SFN_Multiply,id:9034,x:32789,y:33054,varname:node_9034,prsc:2|A-5750-OUT,B-857-OUT;n:type:ShaderForge.SFN_Lerp,id:9009,x:32550,y:33231,varname:node_9009,prsc:2|A-3200-RGB,B-6820-OUT,T-4496-OUT;n:type:ShaderForge.SFN_Vector1,id:6820,x:32365,y:33299,varname:node_6820,prsc:2,v1:1;n:type:ShaderForge.SFN_Color,id:3200,x:32182,y:33278,ptovrint:False,ptlb:Attenuation Color,ptin:_AttenuationColor,varname:node_3200,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.08953287,c2:0.1333015,c3:0.1764706,c4:0.509;n:type:ShaderForge.SFN_SwitchProperty,id:857,x:32723,y:33213,ptovrint:False,ptlb:Cast Shadows,ptin:_CastShadows,varname:node_857,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False|A-7084-OUT,B-9009-OUT;n:type:ShaderForge.SFN_Vector1,id:7084,x:32497,y:33134,varname:node_7084,prsc:2,v1:1;proporder:7241-417-8997-2482-3367-5520-7316-469-3200-857;pass:END;sub:END;*/

Shader "Shader Forge/SailsShader" {
    Properties {
        _Color ("Color", Color) = (0.07843138,0.3921569,0.7843137,1)
        _ColorRamp ("Color Ramp", 2D) = "white" {}
        _FresnelExp ("Fresnel Exp", Range(0, 15)) = 1.572
        _FresnelColor ("Fresnel Color", Color) = (0.3455882,0.8646043,1,1)
        _DisplacementNoise ("Displacement Noise", 2D) = "white" {}
        _OffsetDepth ("Offset Depth", Range(0, 2)) = 1
        _Windspeed ("Wind speed", Range(0, 5)) = 0.4615385
        _NoiseSizeMult ("Noise Size Mult", Range(0, 1)) = 0.2
        _AttenuationColor ("Attenuation Color", Color) = (0.08953287,0.1333015,0.1764706,0.509)
        [MaterialToggle] _CastShadows ("Cast Shadows", Float ) = 1
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Cull Off
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _Color;
            uniform sampler2D _ColorRamp; uniform float4 _ColorRamp_ST;
            uniform float _FresnelExp;
            uniform float4 _FresnelColor;
            uniform sampler2D _DisplacementNoise; uniform float4 _DisplacementNoise_ST;
            uniform float _OffsetDepth;
            uniform float _Windspeed;
            uniform float _NoiseSizeMult;
            uniform float4 _AttenuationColor;
            uniform fixed _CastShadows;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float4 node_9633 = _Time + _TimeEditor;
                float2 node_9849 = ((o.uv0+(node_9633.g*_Windspeed)*float2(1,1))*_NoiseSizeMult);
                float4 _DisplacementNoise_var = tex2Dlod(_DisplacementNoise,float4(TRANSFORM_TEX(node_9849, _DisplacementNoise),0.0,0));
                float node_2730 = (_DisplacementNoise_var.r*_OffsetDepth);
                v.vertex.xyz += float3(node_2730,node_2730,node_2730);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
////// Emissive:
                float node_8544 = 0.5*dot(lightDirection,i.normalDir)+0.5;
                float2 node_5965 = float2(node_8544,0.0);
                float4 node_6043 = tex2D(_ColorRamp,TRANSFORM_TEX(node_5965, _ColorRamp));
                float node_1179 = 0.0;
                float node_6820 = 1.0;
                float3 emissive = (((_Color.rgb*node_6043.rgb)+lerp(float3(node_1179,node_1179,node_1179),lerp(float3(node_1179,node_1179,node_1179),_FresnelColor.rgb,pow(1.0-max(0,dot(i.normalDir, viewDirection)),_FresnelExp)),(1.0 - node_8544)))*lerp( 1.0, lerp(_AttenuationColor.rgb,float3(node_6820,node_6820,node_6820),attenuation), _CastShadows ));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            Cull Off
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _Color;
            uniform sampler2D _ColorRamp; uniform float4 _ColorRamp_ST;
            uniform float _FresnelExp;
            uniform float4 _FresnelColor;
            uniform sampler2D _DisplacementNoise; uniform float4 _DisplacementNoise_ST;
            uniform float _OffsetDepth;
            uniform float _Windspeed;
            uniform float _NoiseSizeMult;
            uniform float4 _AttenuationColor;
            uniform fixed _CastShadows;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float4 node_9633 = _Time + _TimeEditor;
                float2 node_9849 = ((o.uv0+(node_9633.g*_Windspeed)*float2(1,1))*_NoiseSizeMult);
                float4 _DisplacementNoise_var = tex2Dlod(_DisplacementNoise,float4(TRANSFORM_TEX(node_9849, _DisplacementNoise),0.0,0));
                float node_2730 = (_DisplacementNoise_var.r*_OffsetDepth);
                v.vertex.xyz += float3(node_2730,node_2730,node_2730);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 finalColor = 0;
                return fixed4(finalColor * 1,0);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _DisplacementNoise; uniform float4 _DisplacementNoise_ST;
            uniform float _OffsetDepth;
            uniform float _Windspeed;
            uniform float _NoiseSizeMult;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                float4 node_9633 = _Time + _TimeEditor;
                float2 node_9849 = ((o.uv0+(node_9633.g*_Windspeed)*float2(1,1))*_NoiseSizeMult);
                float4 _DisplacementNoise_var = tex2Dlod(_DisplacementNoise,float4(TRANSFORM_TEX(node_9849, _DisplacementNoise),0.0,0));
                float node_2730 = (_DisplacementNoise_var.r*_OffsetDepth);
                v.vertex.xyz += float3(node_2730,node_2730,node_2730);
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
