﻿// Toony Colors Pro+Mobile 2
// (c) 2014,2015 Jean Moreno
// Modified by David Leon (Lince Works)


Shader "Toony Colors Pro 2/LDK/Basic"
{
	Properties
	{
		//TOONY COLORS
		_Color("Color", Color) = (0.5,0.5,0.5,1.0)
		_HColor("Highlight Color", Color) = (0.6,0.6,0.6,1.0)
		_SColor("Shadow Color", Color) = (0.3,0.3,0.3,1.0)

		//DIFFUSE
		_MainTex("Main Texture (RGB)", 2D) = "white" {}

	//TOONY COLORS RAMP
	_RampThreshold("#RAMPF# Ramp Threshold", Range(0,1)) = 0.5
		_RampSmooth("#RAMPF# Ramp Smoothing", Range(0.001,1)) = 0.1

		//BUMP
		_BumpMap("#NORM# Normal map (RGB)", 2D) = "bump" {}

	//SPECULAR
	_SpecColor("#SPEC# Specular Color", Color) = (0.5, 0.5, 0.5, 1)
		_Shininess("#SPEC# Shininess", Range(0.0,2)) = 0.1

		//RIM LIGHT
		_RimColor("#RIM# Rim Color", Color) = (0.8,0.8,0.8,0.6)
		_RimMin("#RIM# Rim Min", Range(0,1)) = 0.5
		_RimMax("#RIM# Rim Max", Range(0,1)) = 1.0


	}

		SubShader
	{
		Tags{ "RenderType" = "Opaque" }
		Cull Off

		CGPROGRAM

#pragma surface surf ToonyColorsCustom noambient
#pragma target 3.0
#pragma glsl


		//================================================================
		// VARIABLES

		fixed4 _Color;
	sampler2D _MainTex;

	sampler2D _BumpMap;
	fixed _Shininess;
	fixed4 _RimColor;
	fixed _RimMin;
	fixed _RimMax;
	float4 _RimDir;

	struct Input
	{
		half2 uv_MainTex;
		half2 uv_BumpMap;
		float3 viewDir;
	};

	//================================================================
	// CUSTOM LIGHTING

	//Lighting-related variables
	fixed4 _HColor;
	fixed4 _SColor;
	float _RampThreshold;
	float _RampSmooth;

	//Custom SurfaceOutput
	struct SurfaceOutputCustom
	{
		fixed3 Albedo;
		fixed3 Normal;
		fixed3 Emission;
		half Specular;
		fixed Gloss;
		fixed Alpha;
	};

	inline half4 LightingToonyColorsCustom(SurfaceOutputCustom s, half3 lightDir, half3 viewDir, half atten)
	{
		s.Normal = normalize(s.Normal);
		fixed ndl = max(0, dot(s.Normal, lightDir)*0.5 + 0.5);

		fixed3 ramp = smoothstep(_RampThreshold - _RampSmooth * 0.5, _RampThreshold + _RampSmooth * 0.5, ndl);
#if !(POINT) && !(SPOT)
		ramp *= atten;
#endif
		_SColor = lerp(_HColor, _SColor, _SColor.a);	//Shadows intensity through alpha
		ramp = lerp(_SColor.rgb,_HColor.rgb,ramp);

		//Specular
		half3 h = normalize(lightDir + viewDir);
		float ndh = max(0, dot(s.Normal, h));
		float spec = pow(ndh, s.Specular*128.0) * s.Gloss * 2.0;
		spec *= atten;
		fixed4 c;
		c.rgb = s.Albedo * _LightColor0.rgb * ramp;
#if (POINT || SPOT)
		c.rgb *= atten;
#endif
		c.rgb *= 2;
		c.rgb += _LightColor0.rgb * _SpecColor.rgb * spec;
		c.a = s.Alpha + _LightColor0.a * _SpecColor.a * spec;
		return c;
	}

	inline half4 LightingToonyColorsCustom_SingleLightmap(SurfaceOutputCustom s, fixed4 color)
	{
		half3 lm = DecodeLightmap(color);

		float lum = Luminance(lm);
#if TCP2_RAMPTEXT
		fixed3 ramp = tex2D(_Ramp, fixed2(lum,lum));
#else
		fixed3 ramp = smoothstep(_RampThreshold - _RampSmooth * 0.5, _RampThreshold + _RampSmooth * 0.5, lum);
#endif
		_SColor = lerp(_HColor, _SColor, _SColor.a);	//Shadows intensity through alpha
		ramp = lerp(_SColor.rgb,_HColor.rgb,ramp);
		lm *= ramp * 2;

		return fixed4(lm, 0);
	}

	inline fixed4 LightingToonyColorsCustom_DualLightmap(SurfaceOutputCustom s, fixed4 totalColor, fixed4 indirectOnlyColor, half indirectFade)
	{
		half3 lm = lerp(DecodeLightmap(indirectOnlyColor), DecodeLightmap(totalColor), indirectFade);

		float lum = Luminance(lm);
#if TCP2_RAMPTEXT
		fixed3 ramp = tex2D(_Ramp, fixed2(lum,lum));
#else
		fixed3 ramp = smoothstep(_RampThreshold - _RampSmooth * 0.5, _RampThreshold + _RampSmooth * 0.5, lum);
#endif
		_SColor = lerp(_HColor, _SColor, _SColor.a);	//Shadows intensity through alpha
		ramp = lerp(_SColor.rgb,_HColor.rgb,ramp);
		lm *= ramp * 2;

		return fixed4(lm, 0);
	}

	inline fixed4 LightingToonyColorsCustom_DirLightmap(SurfaceOutputCustom s, fixed4 color, fixed4 scale, half3 viewDir, bool surfFuncWritesNormal, out half3 specColor)
	{
		UNITY_DIRBASIS
			half3 scalePerBasisVector;

		half3 lm = DirLightmapDiffuse(unity_DirBasis, color, scale, s.Normal, surfFuncWritesNormal, scalePerBasisVector);

		half3 lightDir = normalize(scalePerBasisVector.x * unity_DirBasis[0] + scalePerBasisVector.y * unity_DirBasis[1] + scalePerBasisVector.z * unity_DirBasis[2]);
		half3 h = normalize(lightDir + viewDir);

		float nh = max(0, dot(s.Normal, h));
		float spec = pow(nh, s.Specular * 128.0);

		// specColor used outside in the forward path, compiled out in prepass
		specColor = lm * _SpecColor.rgb * s.Gloss * spec;

		float lum = Luminance(lm);
#if TCP2_RAMPTEXT
		fixed3 ramp = tex2D(_Ramp, fixed2(lum,lum));
#else
		fixed3 ramp = smoothstep(_RampThreshold - _RampSmooth * 0.5, _RampThreshold + _RampSmooth * 0.5, lum);
#endif
		_SColor = lerp(_HColor, _SColor, _SColor.a);	//Shadows intensity through alpha
		ramp = lerp(_SColor.rgb,_HColor.rgb,ramp);
		lm *= ramp * 2;

		return half4(lm, spec);
	}

	//================================================================
	// SURFACE FUNCTION

	void surf(Input IN, inout SurfaceOutputCustom o)
	{
		fixed4 mainTex = tex2D(_MainTex, IN.uv_MainTex);

		o.Albedo = mainTex.rgb * _Color.rgb;
		o.Alpha = mainTex.a * _Color.a;

		//Specular
		o.Gloss = 1;
		o.Specular = _Shininess;
		//Normal map
		o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
		//Rim
		float3 viewDir = normalize(IN.viewDir);
		half rim = 1.0f - saturate(dot(viewDir, o.Normal));
		rim = smoothstep(_RimMin, _RimMax, rim);
		o.Emission += (_RimColor.rgb * rim) * _RimColor.a;
	}

	ENDCG
	}

		Fallback "Diffuse"
		CustomEditor "TCP2_MaterialInspector"
}