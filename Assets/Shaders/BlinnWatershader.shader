// Shader created with Shader Forge v1.37 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.37;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:4013,x:36174,y:31839,varname:node_4013,prsc:2|diff-8839-OUT,spec-1628-RGB,emission-3242-OUT,alpha-8401-OUT,refract-8517-OUT,voffset-9255-OUT;n:type:ShaderForge.SFN_Color,id:1304,x:32504,y:32004,ptovrint:False,ptlb:ColorA,ptin:_ColorA,varname:node_1304,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.2595156,c2:0.3809211,c3:0.7352941,c4:1;n:type:ShaderForge.SFN_Tex2d,id:1780,x:31984,y:32697,varname:node_1780,prsc:2,tex:ea667dc398ef126478fe07b1acef7f59,ntxv:0,isnm:False|UVIN-5414-OUT,TEX-4469-TEX;n:type:ShaderForge.SFN_Tex2d,id:476,x:31984,y:33002,varname:node_476,prsc:2,tex:302179c3143fa694f8ccb5b51dfe5b49,ntxv:0,isnm:False|UVIN-3108-UVOUT,TEX-7948-TEX;n:type:ShaderForge.SFN_Tex2d,id:7247,x:31984,y:32852,varname:node_7247,prsc:2,tex:8617eaf171f738246975680968c8375f,ntxv:0,isnm:False|UVIN-3108-UVOUT,TEX-424-TEX;n:type:ShaderForge.SFN_TexCoord,id:3108,x:30624,y:32395,varname:node_3108,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Multiply,id:5414,x:31631,y:32884,varname:node_5414,prsc:2|A-3108-UVOUT,B-5082-OUT;n:type:ShaderForge.SFN_Slider,id:5082,x:31219,y:32848,ptovrint:False,ptlb:wave tile,ptin:_wavetile,varname:node_5082,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:8.47864,max:25;n:type:ShaderForge.SFN_Multiply,id:8348,x:32202,y:32852,varname:node_8348,prsc:2|A-1780-R,B-7247-R,C-476-R;n:type:ShaderForge.SFN_Tex2dAsset,id:4469,x:31073,y:32230,ptovrint:False,ptlb:Noise1,ptin:_Noise1,varname:node_4469,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:ea667dc398ef126478fe07b1acef7f59,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2dAsset,id:7948,x:31083,y:32408,ptovrint:False,ptlb:Noise2,ptin:_Noise2,varname:node_7948,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:302179c3143fa694f8ccb5b51dfe5b49,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2dAsset,id:424,x:31073,y:32598,ptovrint:False,ptlb:Noise3,ptin:_Noise3,varname:node_424,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:8617eaf171f738246975680968c8375f,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Add,id:4506,x:32498,y:32966,varname:node_4506,prsc:2|A-8348-OUT,B-6055-OUT;n:type:ShaderForge.SFN_Slider,id:6055,x:32148,y:33029,ptovrint:False,ptlb:Opacity_add,ptin:_Opacity_add,varname:node_6055,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.4444444,max:1;n:type:ShaderForge.SFN_Tex2d,id:7120,x:31657,y:32349,varname:node_7120,prsc:2,tex:302179c3143fa694f8ccb5b51dfe5b49,ntxv:0,isnm:False|UVIN-8377-UVOUT,TEX-7948-TEX;n:type:ShaderForge.SFN_Subtract,id:2700,x:31846,y:32349,varname:node_2700,prsc:2|A-7120-R,B-8362-OUT;n:type:ShaderForge.SFN_Vector1,id:8362,x:31657,y:32484,varname:node_8362,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Multiply,id:9255,x:32073,y:32298,varname:node_9255,prsc:2|A-9896-OUT,B-2700-OUT,C-8435-OUT;n:type:ShaderForge.SFN_Slider,id:9896,x:31689,y:32250,ptovrint:False,ptlb:Offset Depth,ptin:_OffsetDepth,varname:node_9896,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-0.5,cur:0.5,max:0.5;n:type:ShaderForge.SFN_Tex2d,id:3036,x:32002,y:32011,ptovrint:False,ptlb:Offset Big Shapes,ptin:_OffsetBigShapes,varname:node_3036,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:af2de2d556869364aa6ae1e840472ef9,ntxv:0,isnm:False|UVIN-8190-UVOUT;n:type:ShaderForge.SFN_Append,id:8882,x:32512,y:32437,varname:node_8882,prsc:2|A-1225-OUT,B-9255-OUT;n:type:ShaderForge.SFN_Panner,id:8190,x:31846,y:31966,varname:node_8190,prsc:2,spu:1,spv:1|UVIN-5942-UVOUT,DIST-2851-OUT;n:type:ShaderForge.SFN_Rotator,id:5942,x:31593,y:31841,varname:node_5942,prsc:2|UVIN-3108-UVOUT,SPD-9960-OUT;n:type:ShaderForge.SFN_Time,id:7625,x:30753,y:31814,varname:node_7625,prsc:2;n:type:ShaderForge.SFN_ValueProperty,id:9960,x:31199,y:31879,ptovrint:False,ptlb:Rotate speed,ptin:_Rotatespeed,varname:node_9960,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Panner,id:3529,x:31343,y:32349,varname:node_3529,prsc:2,spu:1,spv:1|UVIN-2118-OUT,DIST-5917-OUT;n:type:ShaderForge.SFN_Multiply,id:4797,x:32786,y:32525,varname:node_4797,prsc:2|A-8882-OUT,B-9133-OUT;n:type:ShaderForge.SFN_ValueProperty,id:9004,x:32317,y:32749,ptovrint:False,ptlb:Refraction Strength,ptin:_RefractionStrength,varname:node_9004,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.5;n:type:ShaderForge.SFN_ValueProperty,id:6233,x:31289,y:32053,ptovrint:False,ptlb:offset movespeed,ptin:_offsetmovespeed,varname:node_6233,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Multiply,id:2851,x:31567,y:31974,varname:node_2851,prsc:2|A-373-OUT,B-6233-OUT;n:type:ShaderForge.SFN_Sin,id:8345,x:32317,y:32497,varname:node_8345,prsc:2|IN-5900-T;n:type:ShaderForge.SFN_Time,id:5900,x:32143,y:32497,varname:node_5900,prsc:2;n:type:ShaderForge.SFN_Sin,id:373,x:30958,y:31824,varname:node_373,prsc:2|IN-7625-TSL;n:type:ShaderForge.SFN_Multiply,id:9133,x:32493,y:32651,varname:node_9133,prsc:2|A-8345-OUT,B-2921-OUT,C-9004-OUT;n:type:ShaderForge.SFN_Slider,id:2921,x:32160,y:32651,ptovrint:False,ptlb:Refractionstrength sine magnitude,ptin:_Refractionstrengthsinemagnitude,varname:node_2921,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:0.1;n:type:ShaderForge.SFN_Color,id:1628,x:33014,y:31939,ptovrint:False,ptlb:Spec Color,ptin:_SpecColor,varname:node_1628,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Tex2d,id:1452,x:31062,y:32983,varname:node_1452,prsc:2,tex:302179c3143fa694f8ccb5b51dfe5b49,ntxv:0,isnm:False|UVIN-6491-OUT,TEX-7948-TEX;n:type:ShaderForge.SFN_Multiply,id:6491,x:30799,y:32764,varname:node_6491,prsc:2|A-3108-UVOUT,B-6039-OUT;n:type:ShaderForge.SFN_Slider,id:6039,x:30418,y:32757,ptovrint:False,ptlb:Crunchydetail tiling,ptin:_Crunchydetailtiling,varname:node_6039,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:18.62106,max:25;n:type:ShaderForge.SFN_Add,id:1225,x:32354,y:32224,varname:node_1225,prsc:2|A-2856-OUT,B-7120-R;n:type:ShaderForge.SFN_Rotator,id:8377,x:31496,y:32349,varname:node_8377,prsc:2|UVIN-3529-UVOUT,SPD-2424-OUT;n:type:ShaderForge.SFN_Slider,id:2424,x:31202,y:32517,ptovrint:False,ptlb:Refraction rotation,ptin:_Refractionrotation,varname:node_2424,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.3,max:1;n:type:ShaderForge.SFN_Color,id:2599,x:32526,y:32183,ptovrint:False,ptlb:ColorB,ptin:_ColorB,varname:node_2599,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0.9172413,c3:1,c4:1;n:type:ShaderForge.SFN_Lerp,id:9736,x:33173,y:32256,varname:node_9736,prsc:2|A-1304-RGB,B-2599-RGB,T-6702-R;n:type:ShaderForge.SFN_Tex2d,id:6702,x:32819,y:32242,varname:node_6702,prsc:2,tex:8617eaf171f738246975680968c8375f,ntxv:0,isnm:False|UVIN-5601-UVOUT,TEX-424-TEX;n:type:ShaderForge.SFN_Rotator,id:5601,x:32697,y:32229,varname:node_5601,prsc:2|UVIN-3108-UVOUT,SPD-9960-OUT;n:type:ShaderForge.SFN_Slider,id:4817,x:30756,y:33224,ptovrint:False,ptlb:Crunchydetail magnitude,ptin:_Crunchydetailmagnitude,varname:node_4817,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.5235376,max:20;n:type:ShaderForge.SFN_Multiply,id:8435,x:31308,y:33024,varname:node_8435,prsc:2|A-1452-R,B-4817-OUT;n:type:ShaderForge.SFN_Tex2d,id:2061,x:33712,y:31414,varname:node_2061,prsc:2,tex:6b093ba2617d1af4e8b7c6b360be6d34,ntxv:0,isnm:False|UVIN-2953-UVOUT,TEX-7670-TEX;n:type:ShaderForge.SFN_Tex2dAsset,id:7670,x:32994,y:31591,ptovrint:False,ptlb:Foam Mask,ptin:_FoamMask,varname:node_7670,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:6b093ba2617d1af4e8b7c6b360be6d34,ntxv:0,isnm:False;n:type:ShaderForge.SFN_TexCoord,id:4605,x:31780,y:31187,varname:node_4605,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Tex2d,id:4293,x:32233,y:31332,ptovrint:False,ptlb:Foam offset noise,ptin:_Foamoffsetnoise,varname:node_4293,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:8617eaf171f738246975680968c8375f,ntxv:0,isnm:False|UVIN-157-OUT;n:type:ShaderForge.SFN_Panner,id:9705,x:32729,y:31210,varname:node_9705,prsc:2,spu:1,spv:1|UVIN-4605-UVOUT,DIST-6003-OUT;n:type:ShaderForge.SFN_Rotator,id:848,x:33020,y:31326,varname:node_848,prsc:2|UVIN-9705-UVOUT,ANG-4293-G,SPD-2141-OUT;n:type:ShaderForge.SFN_Time,id:5216,x:31735,y:31611,varname:node_5216,prsc:2;n:type:ShaderForge.SFN_Slider,id:4486,x:31958,y:31907,ptovrint:False,ptlb:Foam distort Amount,ptin:_FoamdistortAmount,varname:node_4486,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.05522065,max:1;n:type:ShaderForge.SFN_Multiply,id:157,x:32003,y:31410,varname:node_157,prsc:2|A-4605-UVOUT,B-3645-OUT;n:type:ShaderForge.SFN_Slider,id:3645,x:31611,y:31453,ptovrint:False,ptlb:Foam distort scale,ptin:_Foamdistortscale,varname:node_3645,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:2.220085,max:5;n:type:ShaderForge.SFN_Sin,id:8934,x:31966,y:31611,varname:node_8934,prsc:2|IN-5216-TDB;n:type:ShaderForge.SFN_Multiply,id:6003,x:32517,y:31427,varname:node_6003,prsc:2|A-4293-R,B-6478-OUT;n:type:ShaderForge.SFN_Add,id:6478,x:32420,y:31768,varname:node_6478,prsc:2|A-4486-OUT,B-3396-OUT;n:type:ShaderForge.SFN_Slider,id:2141,x:32568,y:31630,ptovrint:False,ptlb:Foam rotate speed,ptin:_Foamrotatespeed,varname:node_2141,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.4630972,max:1;n:type:ShaderForge.SFN_Clamp,id:3396,x:32233,y:31622,varname:node_3396,prsc:2|IN-8934-OUT,MIN-7337-OUT,MAX-6186-OUT;n:type:ShaderForge.SFN_ValueProperty,id:6186,x:31912,y:31753,ptovrint:False,ptlb:Foam distort time contribution,ptin:_Foamdistorttimecontribution,varname:node_6186,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.1;n:type:ShaderForge.SFN_Subtract,id:7337,x:32147,y:31753,varname:node_7337,prsc:2|A-560-OUT,B-6186-OUT;n:type:ShaderForge.SFN_Vector1,id:560,x:31912,y:31814,varname:node_560,prsc:2,v1:1;n:type:ShaderForge.SFN_Lerp,id:7202,x:33436,y:31127,varname:node_7202,prsc:2|A-4605-UVOUT,B-848-UVOUT,T-6773-OUT;n:type:ShaderForge.SFN_Slider,id:6773,x:32985,y:31207,ptovrint:False,ptlb:Foam distortion end amount,ptin:_Foamdistortionendamount,varname:node_6773,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Lerp,id:683,x:34252,y:31520,varname:node_683,prsc:2|A-4568-OUT,B-2061-RGB,T-9369-R;n:type:ShaderForge.SFN_Vector1,id:4568,x:33902,y:31557,varname:node_4568,prsc:2,v1:0;n:type:ShaderForge.SFN_Tex2d,id:9369,x:33681,y:31689,varname:node_9369,prsc:2,tex:ea667dc398ef126478fe07b1acef7f59,ntxv:0,isnm:False|UVIN-9018-OUT,TEX-4469-TEX;n:type:ShaderForge.SFN_Multiply,id:9018,x:33493,y:31617,varname:node_9018,prsc:2|A-4605-UVOUT,B-6232-OUT;n:type:ShaderForge.SFN_Slider,id:6232,x:33140,y:31676,ptovrint:False,ptlb:Foam opacity tiling,ptin:_Foamopacitytiling,varname:node_6232,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:20;n:type:ShaderForge.SFN_Lerp,id:8229,x:34984,y:31643,varname:node_8229,prsc:2|A-8616-OUT,B-5635-OUT,T-683-OUT;n:type:ShaderForge.SFN_Vector1,id:8616,x:34386,y:31727,varname:node_8616,prsc:2,v1:0;n:type:ShaderForge.SFN_Color,id:8346,x:34408,y:31417,ptovrint:False,ptlb:Foam Color,ptin:_FoamColor,varname:node_8346,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.7801687,c2:0.912257,c3:0.9558824,c4:0.159;n:type:ShaderForge.SFN_Multiply,id:5635,x:34726,y:31446,varname:node_5635,prsc:2|A-8346-RGB,B-8346-A;n:type:ShaderForge.SFN_Panner,id:2953,x:33676,y:31240,varname:node_2953,prsc:2,spu:0,spv:1|UVIN-7202-OUT,DIST-221-OUT;n:type:ShaderForge.SFN_Time,id:5173,x:33176,y:31339,varname:node_5173,prsc:2;n:type:ShaderForge.SFN_Multiply,id:221,x:33410,y:31326,varname:node_221,prsc:2|A-5173-TSL,B-9612-OUT;n:type:ShaderForge.SFN_Vector1,id:9612,x:33258,y:31483,varname:node_9612,prsc:2,v1:0.07;n:type:ShaderForge.SFN_DepthBlend,id:4713,x:35356,y:32498,varname:node_4713,prsc:2|DIST-1570-OUT;n:type:ShaderForge.SFN_Relay,id:4147,x:35587,y:31825,varname:node_4147,prsc:2|IN-8229-OUT;n:type:ShaderForge.SFN_ValueProperty,id:1570,x:35162,y:32532,ptovrint:False,ptlb:Depth Distance,ptin:_DepthDistance,varname:node_1570,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:5;n:type:ShaderForge.SFN_Relay,id:8839,x:35797,y:31745,varname:node_8839,prsc:2|IN-9736-OUT;n:type:ShaderForge.SFN_Relay,id:2981,x:35272,y:32269,varname:node_2981,prsc:2|IN-4506-OUT;n:type:ShaderForge.SFN_Multiply,id:8401,x:35623,y:32245,varname:node_8401,prsc:2|A-2981-OUT,B-4713-OUT;n:type:ShaderForge.SFN_Vector1,id:1466,x:35356,y:32329,varname:node_1466,prsc:2,v1:1;n:type:ShaderForge.SFN_Lerp,id:8865,x:35697,y:32383,varname:node_8865,prsc:2|A-1466-OUT,B-1778-OUT,T-4713-OUT;n:type:ShaderForge.SFN_Vector1,id:1778,x:35326,y:32420,varname:node_1778,prsc:2,v1:0;n:type:ShaderForge.SFN_Add,id:3242,x:35820,y:31948,varname:node_3242,prsc:2|A-4147-OUT,B-8865-OUT;n:type:ShaderForge.SFN_Relay,id:8517,x:35500,y:32116,varname:node_8517,prsc:2|IN-4797-OUT;n:type:ShaderForge.SFN_Multiply,id:2118,x:30835,y:32147,varname:node_2118,prsc:2|A-2782-OUT,B-3108-UVOUT;n:type:ShaderForge.SFN_Slider,id:2782,x:30438,y:32199,ptovrint:False,ptlb:Refraction Map Tile,ptin:_RefractionMapTile,varname:node_2782,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:15;n:type:ShaderForge.SFN_ValueProperty,id:3046,x:30775,y:32021,ptovrint:False,ptlb:Refraction Map Pan Speed,ptin:_RefractionMapPanSpeed,varname:node_3046,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Multiply,id:5917,x:31024,y:31987,varname:node_5917,prsc:2|A-7625-TSL,B-3046-OUT;n:type:ShaderForge.SFN_Multiply,id:2856,x:32269,y:32069,varname:node_2856,prsc:2|A-3036-R,B-2160-OUT;n:type:ShaderForge.SFN_ValueProperty,id:2160,x:32073,y:32204,ptovrint:False,ptlb:Offset Big Shapes Contrbution,ptin:_OffsetBigShapesContrbution,varname:node_2160,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;proporder:4469-7948-424-1304-2599-1628-6055-5082-9896-3036-6233-9960-9004-2782-3046-2424-2921-6039-4817-7670-4293-4486-3645-2141-6186-6773-6232-8346-1570-2160;pass:END;sub:END;*/

Shader "Shader Forge/BlinnWatershader" {
    Properties {
        _Noise1 ("Noise1", 2D) = "white" {}
        _Noise2 ("Noise2", 2D) = "white" {}
        _Noise3 ("Noise3", 2D) = "white" {}
        _ColorA ("ColorA", Color) = (0.2595156,0.3809211,0.7352941,1)
        _ColorB ("ColorB", Color) = (0,0.9172413,1,1)
        _SpecColor ("Spec Color", Color) = (1,1,1,1)
        _Opacity_add ("Opacity_add", Range(0, 1)) = 0.4444444
        _wavetile ("wave tile", Range(0, 25)) = 8.47864
        _OffsetDepth ("Offset Depth", Range(-0.5, 0.5)) = 0.5
        _OffsetBigShapes ("Offset Big Shapes", 2D) = "white" {}
        _offsetmovespeed ("offset movespeed", Float ) = 1
        _Rotatespeed ("Rotate speed", Float ) = 0
        _RefractionStrength ("Refraction Strength", Float ) = 0.5
        _RefractionMapTile ("Refraction Map Tile", Range(0, 15)) = 1
        _RefractionMapPanSpeed ("Refraction Map Pan Speed", Float ) = 1
        _Refractionrotation ("Refraction rotation", Range(0, 1)) = 0.3
        _Refractionstrengthsinemagnitude ("Refractionstrength sine magnitude", Range(0, 0.1)) = 0
        _Crunchydetailtiling ("Crunchydetail tiling", Range(0, 25)) = 18.62106
        _Crunchydetailmagnitude ("Crunchydetail magnitude", Range(0, 20)) = 0.5235376
        _FoamMask ("Foam Mask", 2D) = "white" {}
        _Foamoffsetnoise ("Foam offset noise", 2D) = "white" {}
        _FoamdistortAmount ("Foam distort Amount", Range(0, 1)) = 0.05522065
        _Foamdistortscale ("Foam distort scale", Range(0, 5)) = 2.220085
        _Foamrotatespeed ("Foam rotate speed", Range(0, 1)) = 0.4630972
        _Foamdistorttimecontribution ("Foam distort time contribution", Float ) = 0.1
        _Foamdistortionendamount ("Foam distortion end amount", Range(0, 1)) = 1
        _Foamopacitytiling ("Foam opacity tiling", Range(0, 20)) = 1
        _FoamColor ("Foam Color", Color) = (0.7801687,0.912257,0.9558824,0.159)
        _DepthDistance ("Depth Distance", Float ) = 5
        _OffsetBigShapesContrbution ("Offset Big Shapes Contrbution", Float ) = 1
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        GrabPass{ }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _GrabTexture;
            uniform sampler2D _CameraDepthTexture;
            uniform float4 _TimeEditor;
            uniform float4 _ColorA;
            uniform float _wavetile;
            uniform sampler2D _Noise1; uniform float4 _Noise1_ST;
            uniform sampler2D _Noise2; uniform float4 _Noise2_ST;
            uniform sampler2D _Noise3; uniform float4 _Noise3_ST;
            uniform float _Opacity_add;
            uniform float _OffsetDepth;
            uniform sampler2D _OffsetBigShapes; uniform float4 _OffsetBigShapes_ST;
            uniform float _Rotatespeed;
            uniform float _RefractionStrength;
            uniform float _offsetmovespeed;
            uniform float _Refractionstrengthsinemagnitude;
            uniform float4 _SpecColor;
            uniform float _Crunchydetailtiling;
            uniform float _Refractionrotation;
            uniform float4 _ColorB;
            uniform float _Crunchydetailmagnitude;
            uniform sampler2D _FoamMask; uniform float4 _FoamMask_ST;
            uniform sampler2D _Foamoffsetnoise; uniform float4 _Foamoffsetnoise_ST;
            uniform float _FoamdistortAmount;
            uniform float _Foamdistortscale;
            uniform float _Foamrotatespeed;
            uniform float _Foamdistorttimecontribution;
            uniform float _Foamdistortionendamount;
            uniform float _Foamopacitytiling;
            uniform float4 _FoamColor;
            uniform float _DepthDistance;
            uniform float _RefractionMapTile;
            uniform float _RefractionMapPanSpeed;
            uniform float _OffsetBigShapesContrbution;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 screenPos : TEXCOORD3;
                float4 projPos : TEXCOORD4;
                UNITY_FOG_COORDS(5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float4 node_7614 = _Time + _TimeEditor;
                float node_8377_ang = node_7614.g;
                float node_8377_spd = _Refractionrotation;
                float node_8377_cos = cos(node_8377_spd*node_8377_ang);
                float node_8377_sin = sin(node_8377_spd*node_8377_ang);
                float2 node_8377_piv = float2(0.5,0.5);
                float4 node_7625 = _Time + _TimeEditor;
                float2 node_8377 = (mul(((_RefractionMapTile*o.uv0)+(node_7625.r*_RefractionMapPanSpeed)*float2(1,1))-node_8377_piv,float2x2( node_8377_cos, -node_8377_sin, node_8377_sin, node_8377_cos))+node_8377_piv);
                float4 node_7120 = tex2Dlod(_Noise2,float4(TRANSFORM_TEX(node_8377, _Noise2),0.0,0));
                float2 node_6491 = (o.uv0*_Crunchydetailtiling);
                float4 node_1452 = tex2Dlod(_Noise2,float4(TRANSFORM_TEX(node_6491, _Noise2),0.0,0));
                float node_9255 = (_OffsetDepth*(node_7120.r-0.5)*(node_1452.r*_Crunchydetailmagnitude));
                v.vertex.xyz += float3(node_9255,node_9255,node_9255);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                o.projPos = ComputeScreenPos (o.pos);
                COMPUTE_EYEDEPTH(o.projPos.z);
                o.screenPos = o.pos;
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                #if UNITY_UV_STARTS_AT_TOP
                    float grabSign = -_ProjectionParams.x;
                #else
                    float grabSign = _ProjectionParams.x;
                #endif
                i.normalDir = normalize(i.normalDir);
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float sceneZ = max(0,LinearEyeDepth (UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos)))) - _ProjectionParams.g);
                float partZ = max(0,i.projPos.z - _ProjectionParams.g);
                float4 node_7625 = _Time + _TimeEditor;
                float node_373 = sin(node_7625.r);
                float4 node_7614 = _Time + _TimeEditor;
                float node_5942_ang = node_7614.g;
                float node_5942_spd = _Rotatespeed;
                float node_5942_cos = cos(node_5942_spd*node_5942_ang);
                float node_5942_sin = sin(node_5942_spd*node_5942_ang);
                float2 node_5942_piv = float2(0.5,0.5);
                float2 node_5942 = (mul(i.uv0-node_5942_piv,float2x2( node_5942_cos, -node_5942_sin, node_5942_sin, node_5942_cos))+node_5942_piv);
                float2 node_8190 = (node_5942+(node_373*_offsetmovespeed)*float2(1,1));
                float4 _OffsetBigShapes_var = tex2D(_OffsetBigShapes,TRANSFORM_TEX(node_8190, _OffsetBigShapes));
                float node_8377_ang = node_7614.g;
                float node_8377_spd = _Refractionrotation;
                float node_8377_cos = cos(node_8377_spd*node_8377_ang);
                float node_8377_sin = sin(node_8377_spd*node_8377_ang);
                float2 node_8377_piv = float2(0.5,0.5);
                float2 node_8377 = (mul(((_RefractionMapTile*i.uv0)+(node_7625.r*_RefractionMapPanSpeed)*float2(1,1))-node_8377_piv,float2x2( node_8377_cos, -node_8377_sin, node_8377_sin, node_8377_cos))+node_8377_piv);
                float4 node_7120 = tex2D(_Noise2,TRANSFORM_TEX(node_8377, _Noise2));
                float2 node_6491 = (i.uv0*_Crunchydetailtiling);
                float4 node_1452 = tex2D(_Noise2,TRANSFORM_TEX(node_6491, _Noise2));
                float node_9255 = (_OffsetDepth*(node_7120.r-0.5)*(node_1452.r*_Crunchydetailmagnitude));
                float4 node_5900 = _Time + _TimeEditor;
                float node_9133 = (sin(node_5900.g)*_Refractionstrengthsinemagnitude*_RefractionStrength);
                float2 sceneUVs = float2(1,grabSign)*i.screenPos.xy*0.5+0.5 + (float2(((_OffsetBigShapes_var.r*_OffsetBigShapesContrbution)+node_7120.r),node_9255)*node_9133);
                float4 sceneColor = tex2D(_GrabTexture, sceneUVs);
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = 1;
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float gloss = 0.5;
                float specPow = exp2( gloss * 10.0 + 1.0 );
////// Specular:
                float NdotL = saturate(dot( normalDirection, lightDirection ));
                float3 specularColor = _SpecColor.rgb;
                float3 directSpecular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularColor;
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                float node_5601_ang = node_7614.g;
                float node_5601_spd = _Rotatespeed;
                float node_5601_cos = cos(node_5601_spd*node_5601_ang);
                float node_5601_sin = sin(node_5601_spd*node_5601_ang);
                float2 node_5601_piv = float2(0.5,0.5);
                float2 node_5601 = (mul(i.uv0-node_5601_piv,float2x2( node_5601_cos, -node_5601_sin, node_5601_sin, node_5601_cos))+node_5601_piv);
                float4 node_6702 = tex2D(_Noise3,TRANSFORM_TEX(node_5601, _Noise3));
                float3 diffuseColor = lerp(_ColorA.rgb,_ColorB.rgb,node_6702.r);
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
////// Emissive:
                float node_8616 = 0.0;
                float node_4568 = 0.0;
                float4 node_5173 = _Time + _TimeEditor;
                float2 node_157 = (i.uv0*_Foamdistortscale);
                float4 _Foamoffsetnoise_var = tex2D(_Foamoffsetnoise,TRANSFORM_TEX(node_157, _Foamoffsetnoise));
                float node_848_ang = _Foamoffsetnoise_var.g;
                float node_848_spd = _Foamrotatespeed;
                float node_848_cos = cos(node_848_spd*node_848_ang);
                float node_848_sin = sin(node_848_spd*node_848_ang);
                float2 node_848_piv = float2(0.5,0.5);
                float4 node_5216 = _Time + _TimeEditor;
                float2 node_848 = (mul((i.uv0+(_Foamoffsetnoise_var.r*(_FoamdistortAmount+clamp(sin(node_5216.b),(1.0-_Foamdistorttimecontribution),_Foamdistorttimecontribution)))*float2(1,1))-node_848_piv,float2x2( node_848_cos, -node_848_sin, node_848_sin, node_848_cos))+node_848_piv);
                float2 node_2953 = (lerp(i.uv0,node_848,_Foamdistortionendamount)+(node_5173.r*0.07)*float2(0,1));
                float4 node_2061 = tex2D(_FoamMask,TRANSFORM_TEX(node_2953, _FoamMask));
                float2 node_9018 = (i.uv0*_Foamopacitytiling);
                float4 node_9369 = tex2D(_Noise1,TRANSFORM_TEX(node_9018, _Noise1));
                float node_4713 = saturate((sceneZ-partZ)/_DepthDistance);
                float3 emissive = (lerp(float3(node_8616,node_8616,node_8616),(_FoamColor.rgb*_FoamColor.a),lerp(float3(node_4568,node_4568,node_4568),node_2061.rgb,node_9369.r))+lerp(1.0,0.0,node_4713));
/// Final Color:
                float3 finalColor = diffuse + specular + emissive;
                float2 node_5414 = (i.uv0*_wavetile);
                float4 node_1780 = tex2D(_Noise1,TRANSFORM_TEX(node_5414, _Noise1));
                float4 node_7247 = tex2D(_Noise3,TRANSFORM_TEX(i.uv0, _Noise3));
                float4 node_476 = tex2D(_Noise2,TRANSFORM_TEX(i.uv0, _Noise2));
                fixed4 finalRGBA = fixed4(lerp(sceneColor.rgb, finalColor,(((node_1780.r*node_7247.r*node_476.r)+_Opacity_add)*node_4713)),1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _GrabTexture;
            uniform sampler2D _CameraDepthTexture;
            uniform float4 _TimeEditor;
            uniform float4 _ColorA;
            uniform float _wavetile;
            uniform sampler2D _Noise1; uniform float4 _Noise1_ST;
            uniform sampler2D _Noise2; uniform float4 _Noise2_ST;
            uniform sampler2D _Noise3; uniform float4 _Noise3_ST;
            uniform float _Opacity_add;
            uniform float _OffsetDepth;
            uniform sampler2D _OffsetBigShapes; uniform float4 _OffsetBigShapes_ST;
            uniform float _Rotatespeed;
            uniform float _RefractionStrength;
            uniform float _offsetmovespeed;
            uniform float _Refractionstrengthsinemagnitude;
            uniform float4 _SpecColor;
            uniform float _Crunchydetailtiling;
            uniform float _Refractionrotation;
            uniform float4 _ColorB;
            uniform float _Crunchydetailmagnitude;
            uniform sampler2D _FoamMask; uniform float4 _FoamMask_ST;
            uniform sampler2D _Foamoffsetnoise; uniform float4 _Foamoffsetnoise_ST;
            uniform float _FoamdistortAmount;
            uniform float _Foamdistortscale;
            uniform float _Foamrotatespeed;
            uniform float _Foamdistorttimecontribution;
            uniform float _Foamdistortionendamount;
            uniform float _Foamopacitytiling;
            uniform float4 _FoamColor;
            uniform float _DepthDistance;
            uniform float _RefractionMapTile;
            uniform float _RefractionMapPanSpeed;
            uniform float _OffsetBigShapesContrbution;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 screenPos : TEXCOORD3;
                float4 projPos : TEXCOORD4;
                LIGHTING_COORDS(5,6)
                UNITY_FOG_COORDS(7)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float4 node_3403 = _Time + _TimeEditor;
                float node_8377_ang = node_3403.g;
                float node_8377_spd = _Refractionrotation;
                float node_8377_cos = cos(node_8377_spd*node_8377_ang);
                float node_8377_sin = sin(node_8377_spd*node_8377_ang);
                float2 node_8377_piv = float2(0.5,0.5);
                float4 node_7625 = _Time + _TimeEditor;
                float2 node_8377 = (mul(((_RefractionMapTile*o.uv0)+(node_7625.r*_RefractionMapPanSpeed)*float2(1,1))-node_8377_piv,float2x2( node_8377_cos, -node_8377_sin, node_8377_sin, node_8377_cos))+node_8377_piv);
                float4 node_7120 = tex2Dlod(_Noise2,float4(TRANSFORM_TEX(node_8377, _Noise2),0.0,0));
                float2 node_6491 = (o.uv0*_Crunchydetailtiling);
                float4 node_1452 = tex2Dlod(_Noise2,float4(TRANSFORM_TEX(node_6491, _Noise2),0.0,0));
                float node_9255 = (_OffsetDepth*(node_7120.r-0.5)*(node_1452.r*_Crunchydetailmagnitude));
                v.vertex.xyz += float3(node_9255,node_9255,node_9255);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                o.projPos = ComputeScreenPos (o.pos);
                COMPUTE_EYEDEPTH(o.projPos.z);
                o.screenPos = o.pos;
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                #if UNITY_UV_STARTS_AT_TOP
                    float grabSign = -_ProjectionParams.x;
                #else
                    float grabSign = _ProjectionParams.x;
                #endif
                i.normalDir = normalize(i.normalDir);
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float sceneZ = max(0,LinearEyeDepth (UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos)))) - _ProjectionParams.g);
                float partZ = max(0,i.projPos.z - _ProjectionParams.g);
                float4 node_7625 = _Time + _TimeEditor;
                float node_373 = sin(node_7625.r);
                float4 node_3403 = _Time + _TimeEditor;
                float node_5942_ang = node_3403.g;
                float node_5942_spd = _Rotatespeed;
                float node_5942_cos = cos(node_5942_spd*node_5942_ang);
                float node_5942_sin = sin(node_5942_spd*node_5942_ang);
                float2 node_5942_piv = float2(0.5,0.5);
                float2 node_5942 = (mul(i.uv0-node_5942_piv,float2x2( node_5942_cos, -node_5942_sin, node_5942_sin, node_5942_cos))+node_5942_piv);
                float2 node_8190 = (node_5942+(node_373*_offsetmovespeed)*float2(1,1));
                float4 _OffsetBigShapes_var = tex2D(_OffsetBigShapes,TRANSFORM_TEX(node_8190, _OffsetBigShapes));
                float node_8377_ang = node_3403.g;
                float node_8377_spd = _Refractionrotation;
                float node_8377_cos = cos(node_8377_spd*node_8377_ang);
                float node_8377_sin = sin(node_8377_spd*node_8377_ang);
                float2 node_8377_piv = float2(0.5,0.5);
                float2 node_8377 = (mul(((_RefractionMapTile*i.uv0)+(node_7625.r*_RefractionMapPanSpeed)*float2(1,1))-node_8377_piv,float2x2( node_8377_cos, -node_8377_sin, node_8377_sin, node_8377_cos))+node_8377_piv);
                float4 node_7120 = tex2D(_Noise2,TRANSFORM_TEX(node_8377, _Noise2));
                float2 node_6491 = (i.uv0*_Crunchydetailtiling);
                float4 node_1452 = tex2D(_Noise2,TRANSFORM_TEX(node_6491, _Noise2));
                float node_9255 = (_OffsetDepth*(node_7120.r-0.5)*(node_1452.r*_Crunchydetailmagnitude));
                float4 node_5900 = _Time + _TimeEditor;
                float node_9133 = (sin(node_5900.g)*_Refractionstrengthsinemagnitude*_RefractionStrength);
                float2 sceneUVs = float2(1,grabSign)*i.screenPos.xy*0.5+0.5 + (float2(((_OffsetBigShapes_var.r*_OffsetBigShapesContrbution)+node_7120.r),node_9255)*node_9133);
                float4 sceneColor = tex2D(_GrabTexture, sceneUVs);
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float gloss = 0.5;
                float specPow = exp2( gloss * 10.0 + 1.0 );
////// Specular:
                float NdotL = saturate(dot( normalDirection, lightDirection ));
                float3 specularColor = _SpecColor.rgb;
                float3 directSpecular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularColor;
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float node_5601_ang = node_3403.g;
                float node_5601_spd = _Rotatespeed;
                float node_5601_cos = cos(node_5601_spd*node_5601_ang);
                float node_5601_sin = sin(node_5601_spd*node_5601_ang);
                float2 node_5601_piv = float2(0.5,0.5);
                float2 node_5601 = (mul(i.uv0-node_5601_piv,float2x2( node_5601_cos, -node_5601_sin, node_5601_sin, node_5601_cos))+node_5601_piv);
                float4 node_6702 = tex2D(_Noise3,TRANSFORM_TEX(node_5601, _Noise3));
                float3 diffuseColor = lerp(_ColorA.rgb,_ColorB.rgb,node_6702.r);
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                float2 node_5414 = (i.uv0*_wavetile);
                float4 node_1780 = tex2D(_Noise1,TRANSFORM_TEX(node_5414, _Noise1));
                float4 node_7247 = tex2D(_Noise3,TRANSFORM_TEX(i.uv0, _Noise3));
                float4 node_476 = tex2D(_Noise2,TRANSFORM_TEX(i.uv0, _Noise2));
                float node_4713 = saturate((sceneZ-partZ)/_DepthDistance);
                fixed4 finalRGBA = fixed4(finalColor * (((node_1780.r*node_7247.r*node_476.r)+_Opacity_add)*node_4713),0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Back
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _Noise2; uniform float4 _Noise2_ST;
            uniform float _OffsetDepth;
            uniform float _Crunchydetailtiling;
            uniform float _Refractionrotation;
            uniform float _Crunchydetailmagnitude;
            uniform float _RefractionMapTile;
            uniform float _RefractionMapPanSpeed;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                float4 node_5938 = _Time + _TimeEditor;
                float node_8377_ang = node_5938.g;
                float node_8377_spd = _Refractionrotation;
                float node_8377_cos = cos(node_8377_spd*node_8377_ang);
                float node_8377_sin = sin(node_8377_spd*node_8377_ang);
                float2 node_8377_piv = float2(0.5,0.5);
                float4 node_7625 = _Time + _TimeEditor;
                float2 node_8377 = (mul(((_RefractionMapTile*o.uv0)+(node_7625.r*_RefractionMapPanSpeed)*float2(1,1))-node_8377_piv,float2x2( node_8377_cos, -node_8377_sin, node_8377_sin, node_8377_cos))+node_8377_piv);
                float4 node_7120 = tex2Dlod(_Noise2,float4(TRANSFORM_TEX(node_8377, _Noise2),0.0,0));
                float2 node_6491 = (o.uv0*_Crunchydetailtiling);
                float4 node_1452 = tex2Dlod(_Noise2,float4(TRANSFORM_TEX(node_6491, _Noise2),0.0,0));
                float node_9255 = (_OffsetDepth*(node_7120.r-0.5)*(node_1452.r*_Crunchydetailmagnitude));
                v.vertex.xyz += float3(node_9255,node_9255,node_9255);
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
