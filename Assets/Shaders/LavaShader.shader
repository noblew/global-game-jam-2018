// Shader created with Shader Forge v1.37 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.37;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:32446,y:32343,varname:node_3138,prsc:2|emission-4510-OUT;n:type:ShaderForge.SFN_Color,id:7241,x:31833,y:32456,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_7241,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Multiply,id:2147,x:31996,y:32456,varname:node_2147,prsc:2|A-7241-RGB,B-1099-OUT;n:type:ShaderForge.SFN_ValueProperty,id:1099,x:31833,y:32370,ptovrint:False,ptlb:Brightness,ptin:_Brightness,varname:node_1099,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Lerp,id:4510,x:32082,y:32639,varname:node_4510,prsc:2|A-5025-RGB,B-2147-OUT,T-6042-RGB;n:type:ShaderForge.SFN_Tex2d,id:6042,x:31833,y:32881,ptovrint:False,ptlb:Mask,ptin:_Mask,varname:node_6042,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:6b093ba2617d1af4e8b7c6b360be6d34,ntxv:0,isnm:False|UVIN-2940-UVOUT;n:type:ShaderForge.SFN_Panner,id:2940,x:31621,y:32881,varname:node_2940,prsc:2,spu:0,spv:1|UVIN-9769-UVOUT,DIST-2735-OUT;n:type:ShaderForge.SFN_TexCoord,id:9769,x:31417,y:32710,varname:node_9769,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_ValueProperty,id:5965,x:31205,y:32881,ptovrint:False,ptlb:Pan Speed,ptin:_PanSpeed,varname:node_5965,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:5;n:type:ShaderForge.SFN_Color,id:5025,x:31833,y:32639,ptovrint:False,ptlb:Masked Color,ptin:_MaskedColor,varname:_Color_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Time,id:1443,x:31205,y:32944,varname:node_1443,prsc:2;n:type:ShaderForge.SFN_Multiply,id:2735,x:31417,y:32881,varname:node_2735,prsc:2|A-5965-OUT,B-1443-TSL;proporder:7241-1099-6042-5965-5025;pass:END;sub:END;*/

Shader "Shader Forge/LavaShader" {
    Properties {
        _Color ("Color", Color) = (1,0,0,1)
        _Brightness ("Brightness", Float ) = 1
        _Mask ("Mask", 2D) = "white" {}
        _PanSpeed ("Pan Speed", Float ) = 5
        _MaskedColor ("Masked Color", Color) = (0,0,0,1)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _Color;
            uniform float _Brightness;
            uniform sampler2D _Mask; uniform float4 _Mask_ST;
            uniform float _PanSpeed;
            uniform float4 _MaskedColor;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos( v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float4 node_1443 = _Time + _TimeEditor;
                float2 node_2940 = (i.uv0+(_PanSpeed*node_1443.r)*float2(0,1));
                float4 _Mask_var = tex2D(_Mask,TRANSFORM_TEX(node_2940, _Mask));
                float3 emissive = lerp(_MaskedColor.rgb,(_Color.rgb*_Brightness),_Mask_var.rgb);
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
