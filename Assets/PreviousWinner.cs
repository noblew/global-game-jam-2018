﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class PreviousWinner : MonoBehaviour{
	public static PreviousWinner instance;
    private int previousWinnerInt;
     
     public virtual void Awake() {
		if (instance == null)
           instance = this;
            
            //If instance already exists and it's not this:
            else if (instance != this)
                
                //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
                Destroy(gameObject);    
		previousWinnerInt = 0;
     }

     public void setWinner(int winner) {
		previousWinnerInt = winner;
     }

	public int getWinner() {
		return previousWinnerInt;
     }

}
